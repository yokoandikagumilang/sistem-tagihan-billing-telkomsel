<?php 

/**
* 
*/
class Admin
{
	private $koneksi;
	public $id_admin;
	public $name;
	public $email;
	public $username;
	public $password;
	public $level;
	
	function __construct($db)
	{
		$this->koneksi = $db;
	}

	public function login($username)
  {
    $query = "SELECT * FROM admin WHERE username = ?";
    $stmt  = $this->koneksi->prepare($query);
    $stmt->bindParam(1, $username);
    $stmt->execute();

    $hasil = $stmt->fetch(PDO::FETCH_ASSOC);
    $this->username = $hasil['username'];
    $this->password = $hasil['password'];
    $this->level    = $hasil['level'];
    $this->id = $hasil['id'];
  }
}

?>