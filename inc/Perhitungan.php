<?php 
/**
* 
*/
class Perhitungan
{
	private $koneksi;
	public $id_saw;
	public $no_wisata;
	public $c1;
	public $c2;
	public $c3;
	public $c4;
	
	function __construct($db)
	{
		$this->koneksi = $db;
	}

	public function addperhitungan()
	{
		try{
			$query = "INSERT INTO saw (id_saw, no_wisata, c1, c2, c3, c4) VALUES (:id_saw, :no_wisata, :c1, :c2, :c3, :c4)";
			$stmt = $this->koneksi->prepare($query);
			$stmt->bindParam(':id_saw', $_POST['id_saw']);
			$stmt->bindParam(':no_wisata', $_POST['no_wisata']);
			$stmt->bindParam(':c1',$_POST['c1']);
			$stmt->bindParam(':c2',$_POST['c2']);
			$stmt->bindParam(':c3',$_POST['c3']);
			$stmt->bindParam(':c4',$_POST['c4']);
			$stmt->execute();
		}catch(PDOException $e){
			echo "Gagal :".$e->getMassage();
		}
	}

	public function max()
	{
		try{
			$query = "SELECT MAX(c1) as MAXk1 ,MIN(c2) as MAXk2 ,MAX(c3) as MAXk3 , MAX(c4) as MAXk4 FROM saw";
			$stmt= $this->koneksi->prepare($query);
			$stmt->execute();
			return $stmt;
		} catch (PDOException $e){
            echo "Gagal :".$e->getMassage();
       }
	}

	 public function rubahPer($id_saw)
    {
    	try{
    		$query ="UPDATE saw SET no_wisata = :no_wisata, c1 = :c1, c2 = :c2, c3 = :c3, c4 = :c4 WHERE id_saw = $id_saw";
    		$stmt = $this->koneksi->prepare($query);
    		$stmt->bindParam(':no_wisata', $_POST['no_wisata']);
    		$stmt->bindParam(':c1', $_POST['c1']);
    		$stmt->bindParam(':c2', $_POST['c2']);
    		$stmt->bindParam(':c3', $_POST['c3']);
    		$stmt->bindParam(':c4', $_POST['c4']);
    		$stmt->execute();

    	}catch (PDOException $e){
    		echo "Gagal:".$e->getMassage();
    	}
    }

    public function viewOne($id_saw)
    {
    	try{
    		$query ="SELECT * FROM saw WHERE id_saw = ?";
    		$stmt = $this->koneksi->prepare($query);
    		$stmt->bindParam(1, $id_saw);
    		$stmt->execute();

    		$hasil = $stmt->fetch(PDO::FETCH_ASSOC);
    		$this->id_saw   	= $hasil['id_saw'];
    		$this->no_wisata  	= $hasil['no_wisata'];
    		$this->c1			= $hasil['c1'];
    		$this->c2			= $hasil['c2'];
    		$this->c3			= $hasil['c3'];
    		$this->c4			= $hasil['c4'];
    	}catch (PDOException $e){
    		echo "Gagal :".$e->getMassage();
    	}
    }


    public function lihatsaw()
	{
		try{
			$query = "SELECT * FROM saw ORDER BY no_wisata ASC";
			$stmt = $this->koneksi->prepare($query);
			$stmt->execute();
			return $stmt;
		}catch (PDOException $e){

		}
	}

	public function hapusSaw($table, $id_saw)
	{
		try {
			$query = "DELETE FROM $table WHERE id_saw = ?";
			$stmt = $this->koneksi->prepare($query);
			$stmt->bindParam(1, $id_saw);
			$stmt->execute();
		}catch (PDOException $e){
			echo "Gagal :".$e->getMassage();
		
		}
	}
}

?>