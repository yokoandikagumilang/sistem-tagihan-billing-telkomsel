<?php 

/**
* class fungsi untuk siswa
*/
class Siswa
{
	private $koneksi;
	public $id_siswa;
	public $nis;
	public $nama;
	public $kelas;
	public $alamat;
	public $password;
	public $level;

	
	function __construct($db)
	{
		$this->koneksi = $db;
	}

	public function addSiswa()
	{
		try{
			$query  =  "INSERT INTO siswa ( nis, nama, kelas, alamat, password, level) VALUES (:nis, :nama, :kelas, :alamat, :password, :level)";
			$stmt = $this->koneksi->prepare($query);
			$stmt->bindParam(':nis', $_POST['nis']);
			$stmt->bindParam(':nama', $_POST['nama']);
			$stmt->bindParam(':kelas', $_POST['kelas']);
			$stmt->bindParam(':alamat', $_POST['alamat']);
			$stmt->bindParam(':password', md5($_POST['password']));
			$stmt->bindParam(':level', $_POST['level']);
			$stmt->execute();
		}catch(PDOException $e){

		}
	}

	public function rubahSiswa($nis)
    {
    	try{
    		$query = "UPDATE siswa SET nis = :nis, nama = :nama, kelas =:kelas, alamat = :alamat, password = :password, level= :level WHERE nis = $nis";
    		$stmt = $this->koneksi->prepare($query);
            $stmt->bindParam(':nis', $_POST['nis']);
    		$stmt->bindParam(':nama', $_POST['nama']);
            $stmt->bindParam(':kelas', $_POST['kelas']);
    		$stmt->bindParam(':alamat', $_POST['alamat']);
    		$stmt->bindParam(':password', md5($_POST['password']));
    		$stmt->bindParam(':level', $_POST['level']);
    		$stmt->execute();
    	}catch (PDOException $e){
    		echo "Gagal : ".$e->getMassage();
    	}
    }

	/**
     * Fungsi untuk melihat data user sebelumnya yang akan dirubah
     * @param  string $username $_GET['username'] yang akan dirubah
     * @return void
     */

    public function viewEdit($nis)
    {
    	try{
    		$query = "SELECT * FROM siswa WHERE nis = ?";
    		$stmt = $this->koneksi->prepare($query);
    		$stmt->bindParam(1, $nis);
    		$stmt->execute();

    		$hasil = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->nis = $hasil['nis'];
            $this->nama = $hasil['nama'];
    		$this->kelas = $hasil['kelas'];
            $this->alamat = $hasil['alamat'];
            $this->password = $hasil['password'];
            $this->level = $hasil['level'];

    	}catch (PDOException $e){
    		echo "Gagal :".$e->getMassage();
    	}
    }

	public function daftarSiswa()
	{
		try{
			$query = "SELECT * FROM siswa";
			$stmt=$this->koneksi->prepare($query);
			$stmt->execute();	
			return $stmt;
		}catch(PDOException $e){

		}
	}

	public function lihatsiswa()
	{
		try {
			$query = "SELECT * FROM siswa ORDER BY nis ASC";
			$stmt = $this->koneksi->prepare($query);
			$stmt->execute();

			return $stmt;
			
		} catch (PDOException $e) {
			
		}
	}

	 public function viewNama($nis)
    {
        try{
            $query = "SELECT nama FROM siswa WHERE nis = ?";
            $stmt = $this->koneksi->prepare($query);
            $stmt->bindParam(1, $nis);
            $stmt->execute();

            $hasil = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->nama = $hasil['nama'];
        }catch (PDOException $e){
            echo "Gagal :".$e->getMassage();
        }

        return $this->nama;
    }

     public function masuk($nis)
    { 
        try{
        $query = "SELECT * FROM siswa WHERE nis = ?";
        $stmt  = $this->koneksi->prepare($query);
        $stmt->bindParam(1, $nis);
        $stmt->execute();

        $hasil = $stmt->fetch(PDO::FETCH_ASSOC);
        $this ->nis          = $hasil['nis'];
        $this->password      = $hasil['password'];
        $this->level        = $hasil['level'];
        $this->nis = $hasil['id'];
     }catch (PDOException $e){
                echo "Gagal :".$e->getMassage();
    	}
    }
}
?>