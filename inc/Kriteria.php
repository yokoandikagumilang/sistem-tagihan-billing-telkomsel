<?php 

/**
* fungsi-fungsi untuk kriteria
*/
class Kriteria
{
	private $koneksi;
	public $id_pekerja;
	public $nama;
	public $no_kartu;
	public $fasilitas;
	public $tagihan;
	public $outstanding;
	public $pembayaran;
	public $sisa_tagihan;

	function __construct($db)
	{
		$this->koneksi=	$db;
	}

	public function addkriteria()
	{
		try{
			$query = "INSERT INTO karyawan (nama, no_kartu, fasilitas, tagihan, outstanding, pembayaran, sisa_tagihan) VALUES (:nama, :no_kartu, :fasilitas, :tagihan, :outstanding, :pembayaran, :sisa_tagihan)";
			$stmt = $this->koneksi->prepare($query);
			$stmt->bindParam(':nama', $_POST['nama']); 
			$stmt->bindParam(':no_kartu', $_POST['no_kartu']); 
			$stmt->bindParam(':fasilitas', $_POST['fasilitas']); 
			$stmt->bindParam(':tagihan', $_POST['tagihan']); 
			$stmt->bindParam(':outstanding', $_POST['outstanding']); 
			$stmt->bindParam(':pembayaran', $_POST['pembayaran']); 
			$stmt->bindParam(':sisa_tagihan', $_POST['sisa_tagihan']); 
			$stmt->execute();
		}catch(PDOException $e) {

		}
	}

	public function lihatkriteria()
	{
		try{
			$query = "SELECT * FROM karyawan";
			// var_dump($query); 
			$stmt = $this->koneksi->prepare($query);
			$stmt->execute();
			return $stmt;
		}catch(PDOException $e){

		}
	}

	public function lihatbobot()
      {
     	try{
     		$query = "SELECT bobot FROM kriteria";
     		$stmt = $this->koneksi->prepare($query);
     	$stmt->execute();
     	return $stmt;
     	}catch (PDOException $e){
     	echo "Gagal :".$e->getMassage();
     	}
     }
}

?>