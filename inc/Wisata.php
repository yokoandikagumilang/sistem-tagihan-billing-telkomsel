    <?php 

/**
*class fungsi untuk siswa
*/
class Wisata
{
	private $koneksi;
	public $id_pekerja;
	public $nama;
    public $no_kartu;
    public $fasilitas;
    public $tagihan;
    public $outstanding;
    public $pembayaran;
    public $sisa_tagihan;
	

	
	function __construct($db)
	{
		$this->koneksi = $db;
	}

	public function addSiswa()
	{
		try{
			$query  =  "INSERT INTO wisata ( no_wisata, nama_wisata, keterangan) VALUES (:no_wisata, :nama_wisata, :keterangan)";
			$stmt = $this->koneksi->prepare($query);
			$stmt->bindParam(':no_wisata', $_POST['no_wisata']);
			$stmt->bindParam(':nama_wisata', $_POST['nama_wisata']);
			$stmt->bindParam(':keterangan', $_POST['keterangan']);
			$stmt->execute();
		}catch(PDOException $e){

		}
	}

	public function rubahdata($id_pekerja)
    {
        try{
    		$query = "UPDATE karyawan SET nama =:nama, no_kartu = :no_kartu, fasilitas = :fasilitas, tagihan = :tagihan, outstanding = :outstanding, pembayaran = :pembayaran, sisa_tagihan = :sisa_tagihan, WHERE id_wisata = $id_wisata";
    		$stmt = $this->koneksi->prepare($query);
    		$stmt->bindParam(':nama', $_POST['nama']);
            $stmt->bindParam(':no_kartu', $_POST['no_kartu']);
            $stmt->bindParam(':fasilitas', $_POST['fasilitas']);
            $stmt->bindParam(':tagihan', $_POST['tagihan']);
            $stmt->bindParam(':outstanding', $_POST['outstanding']);
            $stmt->bindParam(':pembayaran', $_POST['pembayaran']);
    		$stmt->bindParam(':sisa_tagihan', $_POST['sisa_tagihan']);
    		$stmt->execute();
    	}catch (PDOException $e){
    		echo "Gagal : ".$e->getMassage();
    	}
    }

	/**
     * Fungsi untuk melihat data user sebelumnya yang akan dirubah
     * @param  string $username $_GET['username'] yang akan dirubah
     * @return void
     */

    public function viewEdit($id_pekerja)
    {
    	try{
    		$query = "SELECT * FROM karyawan WHERE id_pekerja = ?";
    		$stmt = $this->koneksi->prepare($query);
    		$stmt->bindParam(1, $id_pekerja);
    		$stmt->execute();

    		$hasil = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id_pekerja = $hasil['id_pekerja'];
            $this->nama = $hasil['nama'];
    		$this->no_kartu = $hasil['no_kartu'];
            $this->fasilitas = $hasil['fasilitas'];
            $this->tagihan = $hasil['tagihan'];
            $this->outstanding = $hasil['outstanding'];
            $this->pembayaran = $hasil['pembayaran'];
            $this->sisa_tagihan = $hasil['sisa_tagihan'];

    	}catch (PDOException $e){
    		echo "Gagal :".$e->getMassage();
    	}
    }

	public function daftarwisata()
	{
		try{
			$query = "SELECT * FROM wisata";
			$stmt=$this->koneksi->prepare($query);
			$stmt->execute();	
			return $stmt;
		}catch(PDOException $e){

		}
	}

	public function lihatwisata()
	{
		try {
			$query = "SELECT * FROM wisata ORDER BY no_wisata ASC";
			$stmt = $this->koneksi->prepare($query);
			$stmt->execute();

			return $stmt;
			
		} catch (PDOException $e) {
			
		}
	}

	public function viewNama($no_wisata)
    {
        try{
            $query = "SELECT nama_wisata FROM wisata WHERE no_wisata = ?";
            $stmt = $this->koneksi->prepare($query);
            $stmt->bindParam(1, $no_wisata);
            $stmt->execute();

            $hasil = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->nama_wisata = $hasil['nama_wisata'];
        }catch (PDOException $e){
            echo "Gagal :".$e->getMassage();
        }

        return $this->nama_wisata;
    }

    public function viewMasyarakatnya()
    {
        try{
            $query = "SELECT * FROM wisata ORDER BY no_wisata ASC";
            $stmt = $this->koneksi->prepare($query);
            $stmt->execute();
            return $stmt;
        }catch (PDOException $e){
            echo "Gagal :".$e->getMassage();
        }
    }

 

    public function hapuswisata($table, $id_wisata)
    {
    	 try{
            $query ="DELETE FROM $table WHERE id_wisata = ?";
            $stmt= $this->koneksi->prepare($query);
            $stmt->bindParam(1, $id_wisata);
            $stmt->execute();
        }catch (PDOException $e){
            echo "Gagal :".$e->getMassage();
        }
    }
}
?>