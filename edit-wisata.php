<?php
$data = new Wisata($database->koneksi);
$data->viewEdit($_GET['id_pekerja']);
if (isset($_POST['submit'])) {
  $data->rubahdata($_GET['id_pekerja']);
  print_r($data);
  header('location:?halaman=perhitungan');
}
?>

<div class="content-wrapper pull-center">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	Perhitungan
      </h1>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Perhitungan</h3>
            </div>
            <!-- /.box-header -->
            
            <!-- form start -->
            <form role="form" method="POST" accept-charset="utf-8>
              <div class="box-body">
                <div class="form-group">
                	<label for="fasilitas">Fasilitas</label>
                		<input type="text" name="fasilitas" id="no_wisata" class="form-control" disabled value="<?php echo $data->fasilitas;?>">
                	<label for="nama_wisata">Tagihan</label>
                		<input type="text" name="tagihan" class="form-control" id="tagihan" value="<?php echo $data->fasilitas;?>">
                    <label for="pembayaran">Pembayaran</label>
                    <input type="text" name="pembayaran" class="form-control" id="pembayaran" value="<?php echo $data->pembayaran;?>">
                	   
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         </div>
    </div>
  </section>
</div>
