<?php 
$data  = new Perhitungan($database->koneksi);
$data->viewOne($_GET['id_saw']);
if (isset($_POST['submit'])) {
	$data->rubahPer($_GET['id_saw']);
	header('location:?halaman=daftar-saw');
}
?>
<div class="content-wrapper pull-center">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	Seleksi Tempat Wisata
      </h1>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Seleksi</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <form role="form" method="POST" accept-charset="utf-8">
              <div class="box-body">
                <div class="form-group">
                  <label for="no_wisata">Nomor Wisata / Nama Wisata</label>
                   <input type="text" name="no_wisata" class="form-control" id="no_wisata" value="<?php echo $data->no_wisata;?>" >
                  <label for="c1">C1</label>
                    <input type="text" name="c1" class="form-control" id="c1"  value="<?php echo $data->c1;?>">   
                  <label for="c2">c2</label>
                    <input type="text" name="c2" class="form-control" id="c2" value="<?php echo $data->c2;?>">   
                  <label for="c3">c3</label>
                    <input type="text" name="c3" class="form-control" id="c3" value="<?php echo $data->c3;?>">   
                  <label for="c4">c4</label>
                    <input type="text" name="c4" class="form-control" id="c4" value="<?php echo $data->c4;?>">  
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         </div>
    </div>
  </section>
</div>
