<?php 
$data  = new Perhitungan($database->koneksi);
$wisata  = new Wisata($database->koneksi);
$wisata = $wisata->lihatwisata();
$dataperhitungan = $wisata->fetchALL(PDO::FETCH_ASSOC);
if (isset($_POST['submit'])) {
	$data->addperhitungan();
	header('location:?halaman=daftar-saw');
}
?>
<div class="content-wrapper pull-center">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	Seleksi Tempat Wisata
      </h1>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Seleksi</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <form role="form" method="POST" accept-charset="utf-8">
              <div class="box-body">
                <div class="form-group">
                  <label for="no_wisata">Nomor Wisata / Nama Wisata</label>
                    <select name="no_wisata" class="form-control">
                     <option>Pilih Wisata</option>
                    <?php 
                      foreach ($dataperhitungan as $daftarhitung) { ?>
                        <option value="<?php echo $daftarhitung['no_wisata']?>"><?php echo $daftarhitung["no_wisata"]; ?>-<?php echo $daftarhitung["nama_wisata"]; ?></option>
                      <?php } ?>
                        </select>
                  <label for="c1">Lama Perjalanan</label>
                    <select name="c1" class="form-control">
                      <option value="100">1 jam - Nilai 100</option>
                      <option value="80">3 jam Menit - Nilai 80</option>
                      <option value="60">5 jam Menit - Nilai 60</option>
                      <option value="50">1 Hari - Nilai 50</option>
                      <option value="40">> 1 Hari - Nilai 40</option>
                    </select>   
                  <label for="c2">Biaya</label>
                    <select name="c2" class="form-control">
                      <option value="100">300 ribu - nilai 100</option>
                      <option value="80">500 ribu - nilai 80</option>
                      <option value="60">700 ribu - nilai 60</option>
                      <option value="50">1 juta - nilai 50</option>
                      <option value="40">> 1 juta - nilai 40</option>
                    </select> 
                  <label for="c3">Lama Waktu Di Tempat Wisata</label>
                  <select name="c3" class="form-control">
                     <option value="100">1 hari - nilai 100</option>   
                     <option value="80">2 hari - nilai 80</option>   
                     <option value="60">3 hari - nilai 60</option>   
                     <option value="50">4 hari - nilai 50</option>   
                     <option value="40">>4 hari - nilai 40</option>   
                  </select>
                  <label for="c4">Fasilitas</label>
                    <select name="c4" class="form-control">
                      <option value="100">Makan, Minum, Penginapan, Tiket Masuk - Nilai 100</option>
                      <option value="80">Makan, Minum, Penginapan - Nilai 80</option>
                      <option value="60">Makan, Minum - Nilai 60</option>
                      <option value="50">Minum - Nilai 50</option>
                    </select>  
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         </div>
    </div>
  </section>
</div>
