<?php 

$data = new Kriteria($database->koneksi);
$data= $data->lihatkriteria();
print_r($data);
echo '

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Kriteria</h1>
      <a href="?halaman=add-kriteria" type="button"  class="btn btn-success pull-right" >Tambah data</a>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Kriteria</h3>
            </div>
            <!-- /.box-header -->
           
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Nomor Sim Card</th>
                  <th>Fasilitas</th>
                  <th>tagihan</th>
                  <th>Outstanding</th>
                  <th>Pembayaran</th>
                  <th>Sisa Tagihan</th>
               
                </tr>';
                $no=1;
                $datakriteria=$data->fetchAll(PDO::FETCH_ASSOC);
                foreach ($datakriteria as $daftarkriteria) { ?>
                <tr>
                	<td><?php echo $daftarkriteria["id_pekerja"];?></td>
                	<td><?php echo $daftarkriteria["nama"];?></td>
                  <td><?php echo $daftarkriteria["no_kartu"];?></td>
                  <td><?php echo $daftarkriteria["fasilitas"];?></td>
                  <td><?php echo $daftarkriteria["tagihan"];?></td>
                  <td><?php echo $daftarkriteria["outstanding"];?></td>
                  <td><?php echo $daftarkriteria["pembayaran"];?></td>
                	<td><?php echo $daftarkriteria["sisa_tagihan"];?></td>
                </tr>
                	
                <?php $no++ ?>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>';
?>