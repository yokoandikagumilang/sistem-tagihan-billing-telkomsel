<?php 
$data  = new Kriteria($database->koneksi);
if (isset($_POST['submit'])) {
	$data->addkriteria();
	header('location:?halaman="add-kriteria');
}
?>
<div class="content-wrapper pull-center">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	Kriteria
      </h1>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah data</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <form role="form" method="POST" accept-charset="utf-8">
              <div class="box-body">
                <div class="form-group">
                  <label> Nama</label>
                		<input type="text" name="nama" class="form-control" id="nama" placeholder="nama">
                  <label for="type">No Kartu</label>
                    <input type="text" name="no_kartu" class="form-control" id="no_kartu" placeholder="No Kartu">
                  <label for="bobot">Fasilitas</label>
                    <input type="text" name="fasilitas" class="form-control" id="fasilitas" placeholder="Fasilitas" >
                <label for="bobot">Pembayaran</label>
                    <input type="text" name="pembayaran" class="form-control" id="pembayaran" placeholder="Pembayaran" >
                <label for="bobot">Outstanding</label>
                    <input type="text" name="outstanding" class="form-control" id="outstanding" placeholder="Outstanding" >
                <label for="bobot">Tagihan</label>
                    <input type="text" name="tagihan" class="form-control" id="tagihan" placeholder="Tagihan" >
                <label for="bobot">Sisa Tagihan</label>
                    <input type="text" name="sisa_tagihan" class="form-control" id="sisa_tagihan" placeholder="Sisa Tagihan" >
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         </div>
    </div>
  </section>
</div>
