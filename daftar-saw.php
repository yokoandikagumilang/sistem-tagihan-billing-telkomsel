<?php 

$hasil = new Perhitungan($database->koneksi);
$penerima = $hasil->lihatsaw();
$penerimanor = $hasil->lihatsaw();
$penerimarank = $hasil->lihatsaw();
$max = $hasil->max();

$wisata = new Wisata($database->koneksi);

$kriteria = new Kriteria($database->koneksi);
$bobot = $kriteria->lihatbobot();
echo '

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Normalisasi</h1>
    </section>
    <a href="?halaman=add-perhitungan" type="button"  class="btn btn-success pull-right" >Tambah Seleksi</a>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Normalisasi</h3>
            </div>
           
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <div class="box-header">
        
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>no</th>
                  <th>Id Wisata</th>
                  <th>Nama Wisata</th>
                  <th>C1</th>
                  <th>C2</th>
                  <th>C3</th>
                  <th>C4</th>
                  <th>Menu</th>
                </tr>';
                $no=1;
                $baris = $penerima->fetchAll(PDO::FETCH_ASSOC);
                foreach ($baris as $daftarnama) { 
                echo '<tr>';
                echo '<td>'.$no.'</td>';
                echo '<td>'.$daftarnama['no_wisata'].'</td>';
                echo '<td>'.$wisata->viewNama($daftarnama['no_wisata']).'</td>';
                echo '<td>'.$daftarnama['c1'].'</td>';
                echo '<td>'.$daftarnama['c2'].'</td>';
                echo '<td>'.$daftarnama['c3'].'</td>';
                echo '<td>'.$daftarnama['c4'].'</td>';
                echo '<td><a href=?halaman=edit-saw&amp;id_saw='.$daftarnama['id_saw'].' class="btn btn-success"><i class="fa fa-pencil"></i></a><a id="?halaman=hapus-saw&amp;id_saw='.$daftarnama['id_saw'].'" class="btn btn-danger tombol-hapus" ><i class="fa fa-trash"></i></a></td>';
             echo '
                </tr>';
                	
                $no++;
                } 
              echo '</table>
            	</div>
              <a href="?halaman=data-matrix" type="button"  class="btn btn-warning pull-left" >Data Matrix</a>
              <a href="?halaman=rank" type="button"  class="btn btn-primary pull-right" >Prengkingan</a>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div> ';
      echo '
    </section>
    <!-- /.content -->
  </div>';
?>
