
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        </li>
        <li class="treeview">
          <a href="?halaman=daftar-kriteria">
            <i class="fa fa-files-o"></i>
            <span >Data Pekerja</span>
            <span class="label label-primary pull-right"></span>
          </a>    
         </li> 
         <li class="treeview">
          <a href="?halaman=daftar-wisata">
            <i class="fa fa-users"></i>
            <span >Outstanding</span>
            <span class="label label-primary pull-right"></span>
          </a>    
         </li>
         <li class="treeview">
          <a href="?halaman=daftar-saw">
            <i class="fa fa-bar-chart"></i>
            <span >Jumlah Tagihan Sebelumnya</span>
            <span class="label label-primary pull-right"></span>
          </a>    
         </li>
         <li class="treeview">
          <a href="?halaman=hasil">
            <i class="fa fa-tags"></i>
            <span >Jumlah Tagihan Saat Ini</span>
            <span class="label label-primary pull-right"></span>
          </a>    
         </li>
		 <li class="treeview">
          <a href="?halaman=hasil">
            <i class="fa fa-tags"></i>
            <span >Total Kelebihan</span>
            <span class="label label-primary pull-right"></span>
          </a>    
         </li>
    </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  