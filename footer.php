
</body>
 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; 2017 <a href="http://almsaeedstudio.com">Yoko</a>.</strong> All rights
    reserved.
  </footer>
<script src="js/jquery.js"></script>
<script type="text/javascript" src="alert/sweetalert.min.js"></script>
<script type="text/javascript" src="alert/sweetalert-dev.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$('.tombol-hapus').click(function(){
			var tujuan = $(this).attr('id');
			if(confirm("Apakah anda yakin ingin menghapus data ini ?")){
				window.location.href = tujuan;
				swal("Success!", "Data berhasil dihapus", "success");
			}else{
				alert("Hapus di batalkan.");
			}
		});
	});
	</script>
<!-- jQuery 2.2.0 -->
<script src="asset/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="asset/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="asset/js/moment.min.js"></script>

<script src="asset/dist/js/app.min.js"></script>

</body>
</html>
