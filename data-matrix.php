<?php 

$hasil = new Perhitungan($database->koneksi);
$penerima = $hasil->lihatsaw();
$penerimanor = $hasil->lihatsaw();
$penerimarank = $hasil->lihatsaw();
$max = $hasil->max();

$wisata = new Wisata($database->koneksi);
$kriteria = new Kriteria($database->koneksi);
$bobot = $kriteria->lihatbobot();
echo '

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Matrix</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Matrix</h3>
            </div>
           
      <div class="row">
        <div class="col-lg-12">
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>no</th>
                  <th>Id Wisata</th>
                  <th>Nama WIsata</th>
                  <th>C1</th>
                  <th>C2</th>
                  <th>C3</th>
                  <th>C4</th>
                </tr>';
               $no = 1;
              $max = $max->fetchALL(PDO::FETCH_ASSOC);
              $barisan = $penerimanor->fetchALL(PDO::FETCH_ASSOC);
              foreach ($barisan as $daftarpenerima) {
                  echo '<tr>';
                  $nor1 = $daftarpenerima['c1']/$max[0]["MAXk1"];
                  $nor2 = $max[0]["MAXk2"]/$daftarpenerima['c2'];
                  $nor3 = $daftarpenerima['c3']/$max[0]["MAXk3"];
                  $nor4 = $daftarpenerima['c4']/$max[0]["MAXk4"];
                  echo '<td>'.$no.'</td>';
                  echo '<td>'.$daftarpenerima['no_wisata'].'</td>';
                  echo '<td>'.$wisata->viewNama($daftarpenerima['no_wisata']).'</td>';
                  echo '<td>'.round($nor1,2).'</td>';
                  echo '<td>'.round($nor2,2).'</td>';
                  echo '<td>'.round($nor3,2).'</td>';
                  echo '<td>'.round($nor4,2).'</td>';
                  echo '</tr>';
             echo '
                </tr>';
                	
                $no++;
                } 
              echo '</table>
            	</div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div> ';
      echo '
    </section>
    <!-- /.content -->
  </div>';
?>
