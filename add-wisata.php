<?php 
$data  = new Wisata($database->koneksi);
if (isset($_POST['submit'])) {
	$data->addSiswa();
	header('location:?halaman=daftar-wisata');
}
?>
<div class="content-wrapper pull-center">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	Wisata
      </h1>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Wisata</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <form role="form" method="POST">
              <div class="box-body">
                <div class="form-group">
                	<label for="no_wisata">ID Wisata</label>
                		<input type="text" name="no_wisata" id="no_wisata" class="form-control" placeholder="id wisata">
                	<label for="nama_wisata">Nama Wisata</label>
                		<input type="text" name="nama_wisata" class="form-control" id="nama_wisata" placeholder="nama wisata">
                	<label for="keterangan">Keterangan</label>
	                	<textarea type="text" class="form-control" name="keterangan" id="keterangan"></textarea>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
         </div>
    </div>
  </section>
</div>
